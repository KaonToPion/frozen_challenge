from frozen import PolicyEpsilonGreedy
from frozen import LearningFrozenLake
import yaml


if __name__ == '__main__':
    #load config file

    with open('config.yml','r') as r:
        config=yaml.safe_load(r)

    num_episodes=config['num_episodes']
    alpha=config['alpha']
    epsilon=config['epsilon']
    epsilon_decay=config['epsilon_decay']
    discount=config['discount']
    action_space=config['action_space']
    state_space=config['state_space']
    policy_name=config['policy_name']

    policy_model=PolicyEpsilonGreedy(action_space,state_space,epsilon,epsilon_decay)
    a=LearningFrozenLake(num_episodes,alpha,discount)
    a.set_policy(policy_model)
    a.learn_policy()
    a.score_policy()
    a.save_policy('./saved_policies/{}.joblib'.format(policy_name))

