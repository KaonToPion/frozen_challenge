import pytest
import numpy as np
from frozen.policies import PolicyEpsilonGreedy

action_space=4
state_space=64
epsilon=1
epsilon_decay=0.1


class TestPoliciyEpislonGreedy(object):
    
    def test_dict_q_table_generation(self):
        policy=PolicyEpsilonGreedy(action_space,state_space,epsilon,epsilon_decay)
        assert isinstance(policy.Q,dict)
    
    def test_q_update(self):
        policy=PolicyEpsilonGreedy(action_space,state_space,epsilon,epsilon_decay)
        state=5
        action=1
        original_value=policy.Q[state][action]
        value=100
        policy.q_update(state,action,value)
        assert policy.Q[state][action] == original_value+value
    
    def test_q_state(self):
        policy=PolicyEpsilonGreedy(action_space,state_space,epsilon,epsilon_decay)
        state=5
        actions=range(4)
        value_list=[]
        for action in actions:
            value=100+action
            value_list.append(value)
            policy.q_update(state,action,value)
           
        assert pytest.approx(policy.q_state(state)) == value_list

    def test_q_state_action(self):
        policy=PolicyEpsilonGreedy(action_space,state_space,epsilon,epsilon_decay)
        state=5
        actions=range(4)
        for action in actions:
            value=100+action
            policy.q_update(state,action,value)    
            assert policy.q_state_action(state,action) == value

    def test_run_action_values(self):
        policy=PolicyEpsilonGreedy(action_space,state_space,epsilon,epsilon_decay)
        observation=0
        episode=1
        r=policy.selection(observation,episode)
        assert r in range(action_space)
    
    def test_policy_selection_action_values(self):
        policy=PolicyEpsilonGreedy(action_space,state_space,epsilon,epsilon_decay)
        #1
        state=5
        action=0
        policy.Q[state][action]=1
        assert policy.policy_selection(state) == action

        #2
        state=4
        action=3
        policy.Q[state][action]=1
        assert policy.policy_selection(state) == action

        #3
        state=5
        action=2
        policy.Q[state][action]=10
        assert policy.policy_selection(state) == action


    def test_exploratory_selection_action_values(self):
        policy=PolicyEpsilonGreedy(action_space,state_space,epsilon,epsilon_decay)
        #1       
        np.random.seed(0)
        assert policy.exploratory_selection() == 0
        #2 
        np.random.seed(5)
        assert policy.exploratory_selection() == 3
        