import itertools
import sys
import gym
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import joblib


class LearningFrozenLake(object):
    """
    Class to learn the frozen lake environment using q-learning

    """

    def __init__(self, num_episodes=1000, alpha=0.5, discount=0.99, policy=None):
        """


        Args:
            num_episodes (int, optional): . Defaults to 1000.
            alpha (float, optional): learning rate . Defaults to 0.5.
            discount (float, optional):  . Defaults to 0.99.
            policy ([type], optional): policy object. Defaults to None.
        """
        self.env = gym.make("FrozenLake8x8-v1")
        self.num_episodes = num_episodes
        self.alpha = alpha
        self.discount = discount
        self.policy = policy
        # tracking
        self.episode_lengths = np.zeros(self.num_episodes)
        self.episode_rewards = np.zeros(self.num_episodes)

    def set_policy(self, policy):
        """
        Method for setting a policy object

        Args:
            policy ([type]): policy object
        """
        self.policy = policy

    def learn_policy(self):
        """
        Method to learn a policy following q-learning

        Raises:
            Exception: It does need a loaded or set policy
        """

        if self.policy is None:
            raise Exception("A policy should be loaded or set")

        for episode in range(self.num_episodes):
            # Print out which episode we are
            if (episode + 1) % 100 == 0:
                print("\rEpisode {}/{}.".format(episode + 1, self.num_episodes), end="")
                sys.stdout.flush()

            current_state = self.env.reset()

            for time_step in itertools.count():

                # take a new step
                action_choosen = self.policy.selection(current_state, episode)
                next_state, reward, done, _ = self.env.step(action_choosen)
                self.episode_rewards[episode] += reward
                self.episode_lengths[episode] = time_step

                self._q_learning(current_state, next_state, action_choosen, reward)
                if done:
                    break

                current_state = next_state

    def _q_learning(self, current_state: int, next_state: int, action_choosen: int, reward: int):
        """
        Apply q-learning off policy control
        
        Args:
            current_state (int):
            next_state (int):
            action_choosen (int):
            reward (int):
        """
        #Choose the best next action following the policy
        best_next_action = np.argmax(self.policy.q_state(next_state))
        
        # temporal-difference target applying discount to the "prediction" of the action        
        td_target = reward + (self.discount) * self.policy.q_state_action(next_state, best_next_action)
        
        # temporal-difference error subtracting the current state-action value
        td_error = td_target - self.policy.q_state_action(current_state, action_choosen)
        
        # Update the value according to the learning-rate        
        update = self.alpha * td_error
        self.policy.q_update(current_state, action_choosen, update)

    def plot_results(self, smoothing_window=100):
        """
        Method to easily plot results of learning the policy
        Args:
            smoothing_window (int, optional): . Defaults to 100.

        Returns:
            [type]: both figures
        """

        # Plot the episode length over time
        fig1, ax1 = plt.subplots(figsize=(10, 5))
        length_smoothed = (pd.Series(self.episode_lengths).rolling(smoothing_window, min_periods=smoothing_window).mean())
        ax1.plot(length_smoothed)
        ax1.set_xlabel("Episode")
        ax1.set_ylabel("Episode length (smoothed)")
        ax1.set_title("Episode length over time (smoothed over window size {})".format(smoothing_window))

        # Plot the episode reward over time
        fig2, ax2 = plt.subplots(figsize=(10, 5))
        rewards_smoothed = (pd.Series(self.episode_rewards).rolling(smoothing_window, min_periods=smoothing_window).mean())
        ax2.plot(rewards_smoothed)
        ax2.set_xlabel("Episode")
        ax2.set_ylabel("Episode reward (smoothed)")
        ax2.set_title("Episode reward over time (smoothed over window size {})".format(smoothing_window))

        return fig1, fig2

    def score_policy(self, episodes=1000):
        """
        Method to score a policy against the environment, without learning
        Args:
            episodes (int, optional): . Defaults to 1000.

        Raises:
            Exception: This method needs a policy loaded or set
        """
        if self.policy is None:
            raise Exception("A policy should be loaded or set")
        misses = 0
        steps_list = []
        for episode in range(episodes):
            observation = self.env.reset()
            steps = 0
            while True:
                action = self.policy.policy_selection(observation)
                observation, reward, done, _ = self.env.step(action)
                steps += 1
                if done and reward == 1:
                    steps_list.append(steps)
                    break
                elif done and reward == 0:
                    misses += 1
                    break
        print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        print("the agent took an average of {:.0f} steps to finish".format(np.mean(steps_list)))
        print("the agent fell in the hole {:.2f} % of the times".format((misses / episodes) * 100))
        print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")

    def save_policy(self, file_name="policy.joblib"):
        """
        Method to save a policy as joblib file
        Args:
            file_name (str, optional): path or name of the file. Defaults to 'policy.joblib'.

        Raises:
            Exception: This method needs a policy set or loaded
        """

        if self.policy is None:
            raise Exception("A policy should be loaded or set")

        joblib.dump(self.policy, file_name)

    def load_policy(self, file_name="policy.joblib"):
        """
        Method to load a policy from a joblib file
        Args:
            file_name (str, optional): path to joblib file. Defaults to 'policy.joblib'.
        """

        self.policy = joblib.load(file_name)
