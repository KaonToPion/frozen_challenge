import numpy as np


class PolicyEpsilonGreedy(object):
    """
    Epsilon Greedy policy

    """

    def __init__(self, action_space=4, state_space=64, epsilon=1, epsilon_decay=0.99):
        """
        Initilization of epsilon greedy policy
        Args:
            action_space (int, optional):. Defaults to 4.
            state_space (int, optional): . Defaults to 64.
            epsilon (int, optional): . Defaults to 1.
            epsilon_decay (float, optional):. Defaults to 0.99.
        """
        self.action_space = action_space
        self.state_space = state_space
        self.Q = self._q_table_generation()
        self.epsilon = epsilon
        self.epsilon_decay = epsilon_decay

    def _q_table_generation(self) -> dict:
        """
        Method to generate the Q table

        Returns:
            dict: Q table
        """
        # Action-value function
        Q = dict()
        for state in range(self.state_space):
            Q[state] = np.zeros(self.action_space)
        return Q

    def q_update(self, state: int, action: int, update: float):
        """
        Method to update an Action-State value of the Q table

        Args:
            state (int):
            action (int):
            update (float):
        """
        self.Q[state][action] += update

    def q_state(self, state: int):
        """
        Method to get the values of all actions in a state in the q table

        Args:
            state (int):

        Returns:
            np.array: Array of values for each action in this state
        """
        return self.Q[state]

    def q_state_action(self, state: int, action: int) -> float:
        """
        Method to get the value of a certain set of Action-State in the q table

        Args:
            state (int): [description]
            action (int): [description]

        Returns:
            float: Action-State value from a certain position in q-table
        """

        return self.Q[state][action]

    def selection(self, obs: int, n_episode: int) -> int:
        """
        Method for selecting the following action
        Args:
            obs (int): Actual state
            n_episode (int): episode number to control exploration

        Returns:
            int: selected action
        """

        if np.random.rand() < (self.epsilon * (self.epsilon_decay ** n_episode)):
            selected_action = self.exploratory_selection()
        else:
            selected_action = self.policy_selection(obs)
        return selected_action

    def policy_selection(self, obs: int) -> int:
        """
        Method to follow explotation of the policy

        Args:
            obs (int): current state

        Returns:
            int: selected action
        """

        selected_action = np.argmax(self.Q[obs])
        return selected_action

    def exploratory_selection(self) -> int:
        """
        Method to explore the env
        Returns:
            int: selected action
        """
        selected_action = np.random.choice(range(self.action_space))
        return selected_action
