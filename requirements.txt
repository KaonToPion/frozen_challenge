gym==0.21.0
joblib==1.1.0
matplotlib==3.5.0
numpy==1.21.4
pandas==1.3.4
PyYAML==6.0

