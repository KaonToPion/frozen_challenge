# q-learning-frozen-lake

This repository provides the code necesary to solve the gym openai frozen lake environment:
https://gym.openai.com/envs/FrozenLake8x8-v0/
(It's actually solving -v1 of this environment, but this doesn't have a proper webpage to show)

I have solved it using q-learning :

![Alt text](pics/q-learning.png?raw=true "q-learning")

### How to:

You can use the requirements.txt to load the required libraries
```
pip install -r requirements.txt
```
#### Run
You can follow two approaches:

- You can use the the notebook `explore_run.ipynb` to play and check the results on the plots. The configuration is inside the notebook.

- In order to run the python script `run.py` you can modify the configuration file `config.py` and do `python run.py`. This will produce a new policy saved in the saved_policies folder

A Dockerfile is provided as well in order to easily run `run.py`.

### Saved policies folder:

In this folder you can find a saved policy called `policy_11.joblib` with the following result obtained.
s
![Alt text](pics/episodes_trained_policy.png?raw=true "Trained policy plot")

```
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
the agent took an average of 90 steps to finish
the agent fell in the hole 17.20 % of the times
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

````
This was the configuration used:
```
num_episodes=200000
alpha=0.75
epsilon=1
epsilon_decay=0.99995
discount=0.95
action_space=4 # this should not be modified
state_space=8*8 # this should not be modified
```
It's the suggested place to store new policies

### Testing:

Some tests are provided, although not much.
In order to run them I suggest install `test_requirements.txt`.
They use `pytest` as testing framework
